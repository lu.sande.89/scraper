echo "Reiniciando el scraper..."
echo "Eliminando contenedores..."
docker-compose down --remove-orphans
echo "Creando contenedores..."
docker-compose up -d
echo "Scraper reiniciado..."