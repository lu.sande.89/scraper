
import uvicorn
from fastapi import FastAPI

from scraper.scraper.main import idealista, init

app = FastAPI()


@app.get("/")
def root():
    '''Api home'''
    return {"Hello": "World"}


@app.get("/alive")
def alive():
    '''Api home'''
    return {"message": "The API is alive"}


@app.get("/idealista")
def get_idealista():
    init()
    return {"message": "The quotes were obtained"}

#os.system(f"scrapy crawl -L {scrapy_log} idealista_proxy -o ./data/{scrapy_rs_name}.json -a start_urls={url_last_flats}")

if __name__ == "__main__":
    uvicorn.run(
            app="app:app",
            host='0.0.0.0',
            port=5000
        )
