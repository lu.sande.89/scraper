import json
import logging
import os
import platform
import subprocess
import sys
import time
import urllib
import uuid

import telebot

import db_module
from logger import Log
from services import get_urls

__version__ = "1.0.0"
logger = Log("idealista", log_path="./scraper/logs")


def get_config():
    # Si no hay archivo de configuración, obtenemos los parametros del front
    if not os.path.isfile('./data/config.json'):
        if not os.path.exists('data'):
            os.makedirs('data')
        pid = init_app_flask()  # Inicio Flask en localhost:8080
        get_config_flask(pid)
    else:
        with open('./data/config.json') as json_file:
            data = json.load(json_file)
    return data


def check_config(data, db_client, db_name):
    tb = telebot.TeleBot('5042109408:AAHBrCsNiuI3lXBEiLjmyxqXapX4h1LHbJs')

    if not os.path.exists("scrapy.cfg"):
        logger.error("NOT FILE FOUND scrapy.cfg")
        sys.exit()

    # Check urls
    urls = get_urls(data)
    urls_ok = ''
    urls_text = ''
    db_urls = ''
    urls_ok_count = 0

    for portal in urls:
        for url in urls[portal]:
            if len(url.split('/')) > 2:
                portal_url = url.split('/')[2]
                portal_name = portal_url.split('.')[1]
                urls_ok_count += 1
                urls_ok += f' <a href="{url}">{portal_name}</a>    '
                db_urls += f'{url};'
                try:
                    urls_text += f"\t\t- {portal_name} → {url.split('/')[4]}\n"
                except:
                    urls_text += f"\t\t- {portal_name} → {url.split('/')[3]}\n"

    if not data['telegram_chatuserID'] is None:
        try:
            if data['start_msg'] == 'True':
                info_message = tb.send_message(data['telegram_chatuserID'],
                                                f"<code>LOADING...</code>\n"
                                                f"\n"
                                                f"<code>scrapyrealestate v{__version__}\n</code>"
                                                f"\n"
                                                f"<code>REFRESH     <b>{data['time_update']}</b>s</code>\n"
                                                f"<code>MIN PRICE   <b>{data['min_price']}€</b></code>\n"
                                                f"<code>MAX PRICE   <b>{data['max_price']}€</b> (0 = NO LIMIT)</code>\n"
                                                f"<code>URLS        <b>{urls_ok_count}</b>  →   </code>{urls_ok}\n",
                                                parse_mode='HTML'
                )
            else:
                info_message = tb.send_message(data['telegram_chatuserID'],
                                                f"LOADING... scrapyrealestate v{__version__}\n")
        except telebot.apihelper.ApiTelegramException:
            logger.error('TELEGRAM CHAT ID IS NOT CORRECT OR BOT @scrapyrealestatebot NOT ADDED CORRECTLY TO CHANNEL')
            sys.exit()

        data_host = {
            'id': str(uuid.uuid4())[:8],
            'chat_id': info_message.chat.id,
            'group_name': info_message.chat.title,
            'refresh': data['time_update'],
            'min_price': data['min_price'],
            'max_price': data['max_price'],
            'urls': db_urls,
            'host_name': platform.node(),
            'connections': 0,
            'so': platform.platform()
        }

        logger.info(f"TELEGRAM {info_message.chat.title} CHANNEL VERIFIED")

        query_dbcon = db_module.query_host_mongodb(db_client, db_name, 'sr_connections', data_host, logger)
        if not len(query_dbcon) > 0:
            db_module.insert_host_mongodb(db_client, db_name, 'sr_connections', data_host, logger)
        else:
            db_module.update_host_mongodb(db_client, db_name, 'sr_connections', query_dbcon[0], logger)

    else:
        logger.error('TELEGRAM CHAT ID IS EMPTY')
        sys.exit()

    return info_message


def checks(data, config_db_mongodb):
    if int(data['time_update']) < 300:
        logger.error("TIME UPDATE < 300")
        sys.exit()
    time.sleep(0.05)
    db_client = db_module.check_bbdd_mongodb(config_db_mongodb, logger)  # Compruebo conexiona db
    info_message = check_config(data, db_client, config_db_mongodb['db_name'])  # Compruebo parametros de configuracion
    return db_client, info_message


def check_url(url):
    try:
        url_code = urllib.request.urlopen(url).getcode()
    except:
        url_code = 404

    return url_code


def init_app_flask():
    # comprobamos si el servidor está encendido.
    # si no encontramos ninguna página en localhost:8080 ejecutamos el servidor
    localhost_code = check_url("http://localhost:8080")
    if localhost_code != 200:
        try:
            proces_server = subprocess.Popen('python scraper/flask_server.py &', shell=True)
        except:
            proces_server = subprocess.Popen('python3 scraper/flask_server.py &', shell=True)
        pid = proces_server.pid
        real_pid = pid + 1  # +1 porque el pid real siempre es un numero mas
    else:
        real_pid = os.popen('pgrep python scraper/flask_server.py').read()

    return real_pid


def get_config_flask(pid):
    while True:
        try:
            # Si encontramos info en localhost:8080/data guardamos los datos y salimos del bucle
            with open('./data/config.json') as json_file:
                data = json.load(json_file)
                os.system(f'kill {pid}')
                break
        except:
            pass
        time.sleep(1)
