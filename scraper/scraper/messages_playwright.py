from playwright.sync_api import sync_playwright
import re
from playwright.sync_api import Page, expect
import subprocess

def get_browser_context():
    with sync_playwright() as p:
        browser = p.chromium.launch()
        context = browser.new_context()
        return context


def send_message_to_advertiser2(link='https://www.idealista.com/inmueble/102069477/', telegram_msg='', logger=None):
    with sync_playwright() as p:
        browser = p.chromium.launch()
        context = browser.new_context()
        page = context.new_page()

        try:
            page.goto(link)

            # Asegurarse de que la página se haya cargado completamente antes de interactuar con ella
            page.wait_for_load_state()

            # Suponiendo que el mensaje se envía a través de un formulario con un campo de texto y un botón de enviar.
            # Ajusta esto según la estructura de la página web específica.
            logger.info(page.title())
            # message_input = page.locator('input[name="message"]')
            # send_button = page.locator('button[type="submit"]')

            # message_input.fill(telegram_msg)
            # send_button.click()

            # Esperar una respuesta o cargar una nueva página si es necesario.

            logger.info(f"Mensaje enviado al anunciante en el enlace: {link}")

        except Exception as e:
            logger.error(f"Error al enviar el mensaje al anunciante en el enlace: {link}. Detalles: {str(e)}")

        finally:
            context.close()


def send_message_to_advertiser(page: Page, logger=None):

    logger.info("Entro a send_message_to_advertiser")

    page.goto("https://playwright.dev/")

    # Expect a title "to contain" a substring.
    expect(page).to_have_title(re.compile("Playwright"))

    # create a locator
    get_started = page.get_by_role("link", name="Get started")

    # Expect an attribute "to be strictly equal" to the value.
    expect(get_started).to_have_attribute("href", "/docs/intro")

    # Click the get started link.
    get_started.click()

    # Expects the URL to contain intro.
    expect(page).to_have_url(re.compile(".*intro"))

    logger.info("Salgo a send_message_to_advertiser")

def instalar_navegadores():
    # Ejecutar el comando 'playwright install' desde la consola
    subprocess.run(["playwright", "install"], check=True)


def enviar_mensaje_idealista(url='https://www.idealista.com/inmueble/102069477/', mensaje='test'):
    instalar_navegadores()
    with sync_playwright() as p:
        browser = p.chromium.launch()
        page = browser.new_page()

        # Navegar a la página del inmueble
        page.goto(url)

        # Esperar a que la página cargue completamente (puedes ajustar el tiempo de espera)
        page.wait_for_load_state('networkidle')

        # Encontrar el botón o elemento para iniciar el envío de mensaje
        boton_enviar_mensaje = page.locator('selector_del_boton_de_mensaje')
        boton_enviar_mensaje.click()

        # Esperar a que aparezca el campo de mensaje y escribir en él
        campo_mensaje = page.locator('selector_del_campo_mensaje')
        campo_mensaje.fill(mensaje)

        # Enviar el mensaje
        boton_enviar = page.locator('selector_del_boton_enviar')
        boton_enviar.click()

        # Cerrar el navegador
        browser.close()

# Ejemplo de uso
url_inmueble = 'https://www.idealista.com/inmueble/123456789'
mensaje_a_propietario = 'Hola, estoy interesado en su propiedad. ¿Podemos concertar una visita?'
