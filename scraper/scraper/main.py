import json
import logging
import os
import os.path
import platform
import random
import subprocess
import sys
import time
import traceback
import urllib.request
import uuid
from os import path

import telebot
from art import *
from unidecode import unidecode

import db_module as db_module
from config import get_config, checks
from services import scrap_realestate
from logger import Log


ruta_proyecto = "./scraper/scraper/"
__author__ = "mferark"
__license__ = "GPL"
__version__ = "2.0.5"
logger = Log("idealista", log_path="./scraper/logs")


def init():
    global config_db_mongodb
    config_db_mongodb = {
        'db_user': "scrapyrealestate",
        'db_password': "23sjz0UJdfRwsIZm",
        'db_host': "scrapyrealestate.sk0pae1.mongodb.net",
        'db_name': f"scrapyrealestate{__version__.replace('.', '')}",
    }
    print('LOADING...')
    time.sleep(1)
    print(f'scrapyrealestate v{__version__}')
    tprint("scrapyrealestate")
    print(f'scrapyrealestate v{__version__}')

    time.sleep(0.05)
    data = get_config()
    time.sleep(0.05)
    db_client, info_message = checks(data, config_db_mongodb)
    time.sleep(0.05)
    count = 0
    telegram_msg = False
    scrapy_rs_name = data['scrapy_rs_name'].replace("-", "_")
    send_first = data['send_first']

    while True:
        try:
            os.remove(f"./data/{scrapy_rs_name}.json")
        except:
            pass

        if send_first == 'True' or count > 0:
            telegram_msg = True
            logger.debug('TELEGRAM MSG ENABLED')

        scrap_realestate(data, db_client, telegram_msg, logger)

        count += 1  # Sumem 1 cicle
        logger.info(f"SLEEPING {data['time_update']} SECONDS")
        time.sleep(int(data['time_update']))


if __name__ == "__main__":
    init()
