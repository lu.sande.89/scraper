import os
import sys
import subprocess
import time
import random

import telebot
import json
from unidecode import unidecode

import db_module as db_module
from flask_server import get_db


def mix_list(original_list):
    list = original_list[:]
    longitud_list = len(list)
    for i in range(longitud_list):
        index_random = random.randint(0, longitud_list - 1)
        temporal = list[i]
        list[i] = list[index_random]
        list[index_random] = temporal
    return list


def check_new_flats(json_file_name, scrapy_rs_name, min_price, max_price, tg_chatID, db_client, db_name, telegram_msg, logger):
    '''
    Verifica si alguna propiedad no está en la base de datos y la envía por telegram.
    '''
    tb = telebot.TeleBot('5042109408:AAHBrCsNiuI3lXBEiLjmyxqXapX4h1LHbJs')

    new_urls = []

    json_file = open(json_file_name)

    try:
        data_json = json.load(json_file)
    except:
        data_json = ""

    if len(data_json) == 0:
        logger.warning(f'NO DATA IN JSON {scrapy_rs_name.upper()}')
    json_file.close()

    try:
        with open("./data/ids.json", "r") as outfile:
            ids_file = json.load(outfile)
            new_ids_file = []
        outfile.close()
    except FileNotFoundError:
        ids_file = []
        new_ids_file = []
        pass

    for flat in data_json:
        logger.info(flat)
        flat_id = int(flat['id'])
        title = flat["title"].replace("\'", "")
        town = flat.get('town', '')
        neighbour = flat.get('neighbour', '')
        street = flat.get('street', '')
        number = flat.get('number', '')
        type = flat.get('type', '')
        price_str = flat.get('price', 0)

        try:
            price = int(''.join(char for char in flat['price'] if char.isdigit()))
        except:
            price = 0

        if price == 0:
            price = price_str

        try:
            rooms = int(''.join(char for char in flat['rooms'] if char.isdigit()))
        except:
            try: rooms = flat['rooms']
            except: rooms = 0
        try:
            m2 = int(''.join(char for char in flat['m2'] if char.isdigit())[:-1])
            m2_tg = f'{m2}m²'
        except:
            try:
                m2 = flat['m2']
                m2_tg = f'{m2}m²'
            except:
                m2 = 0
                m2_tg = ''

        floor = flat.get('floor', '')
        href = flat['href']
        site = flat['site']

        # Si el id del piso no está en los ids del json:
        if not flat_id in ids_file:
            new_ids_file.append(flat_id)
            data_flat = {
                'id': flat_id,
                'price': price,
                'm2': m2,
                'rooms': rooms,
                'floor': floor,
                'town': town,
                'neighbour': neighbour,
                'street': street,
                'number': number,
                'title': title,
                'href': href,
                'type': type,
                'site': site,
                'online': False
            }
            # Guardamos el piso en la bbdd de mongo #  db.sr_flats.createIndex({id: 1},{unique: true})
            if town != '':
                town_nf = unidecode(town.replace(' ', '_').lower())
                # Comparamos si hay viviendas iguales a mongodb:
                # Viviendas iguales significa que población, precio, m2, num. habitaciones son iguales.
                # También deben ser de otro site, es decir, si estamos comparando un piso que está a idealista, buscaremos en pisoscom, habitaclia y fotocasa
                match_flat_list = db_module.query_flat_mongodb(db_client, db_name, town_nf, data_flat, logger)
                if len(match_flat_list) > 0:
                    pass
                else:
                    if not site == 'fotocasa':
                        try:
                            logger.info("entro al try para guardar data en mongo")
                            mongodb = get_db()
                            saved = mongodb.insert_one(data_flat)
                            logger.info(saved)
                        except Exception as e:
                            logger.info(e)
                        db_module.insert_flat_mongodb(db_client, db_name, town_nf, data_flat, logger)
            if price == 'Aconsultar':
                continue
            elif price == 'A consultar':
                continue

            #? Envío mensaje por telegram (finalizado el mvp, sacar)
            if int(max_price) >= int(price) >= int(min_price) or int(max_price) == 0 and int(price) >= int(min_price):
                if telegram_msg:
                    new_urls.append(href)
                    try: mitja_price_m2 = '%.2f' % (price / float(m2)) # Formatem tg del preu, m2, mitjana i href
                    except:
                        mitja_price_m2 = ''
                    tb.send_message(tg_chatID, f"<b>{price_str}</b> [{m2_tg}] → {mitja_price_m2}€/m²\n{href}", parse_mode='HTML')
                    logger.debug(f'{href} SENT TO TELEGRAM GROUP')
                    time.sleep(3.05)
                time.sleep(0.10)

    # open file in write mode
    with open("./data/ids.json", "w") as outfile:
        json.dump(ids_file+new_ids_file, outfile)
    outfile.close()
    if len(new_urls) > 0:
        logger.info(f"SPIDER FINISHED - [NEW: {len(new_urls)}] [TOTAL: {len(data_json)}]: {new_urls}")
    else:
        logger.debug(f"SPIDER FINISHED - [NEW: {len(new_urls)}] [TOTAL: {len(data_json)}]: {new_urls}")


def get_urls(data):
    urls = {}

    # if data.get('url_idealista', '') == '' and data.get('url_pisoscom', '') == '' and data.get('url_fotocasa', '') == '' and data.get(
    #     'url_habitaclia', '') == '':
    if data.get('url_idealista', '') == '':
        print("NO URLS ENTERED")
        sys.exit()

    start_urls_idealista = data.get('url_idealista', [])
    start_urls_idealista = [url + '?ordenado-por=fecha-publicacion-desc' for url in start_urls_idealista]

    # start_urls_pisoscom = data.get('url_pisoscom', [])
    # start_urls_pisoscom = [url + 'fecharecientedesde-desc/' for url in start_urls_pisoscom]

    # start_urls_fotocasa = data.get('url_fotocasa', [])

    # start_urls_habitaclia = data.get('url_habitaclia', [])
    # start_urls_habitaclia = [url + '?ordenar=mas_recientes' for url in start_urls_habitaclia]

    urls['start_urls_idealista'] = start_urls_idealista
    # urls['start_urls_pisoscom'] = start_urls_pisoscom
    # urls['start_urls_fotocasa'] = start_urls_fotocasa
    # urls['start_urls_habitaclia'] = start_urls_habitaclia

    return urls


def scrap_realestate(data, db_client, telegram_msg, logger):
    start_time = time.time()
    scrapy_rs_name = data['scrapy_rs_name']
    scrapy_log = data['log_level_scrapy'].upper()
    proxy_idealista = data['proxy_idealista']

    urls = []
    for key in data:
        if "url" in key and isinstance(data[key], list):
            urls += data[key]
        elif "url" in key:
            urls.append(data[key])

    # Mezclamos las urles para no repetir la misma spider
    urls_mixed = mix_list(urls)

    # Iteramos las urls que hay y scrapeamos
    for url in urls_mixed:
        if url == '':
            continue

        portal_url = url.split('/')[2]
        portal_name = portal_url.split('.')[1]
        try:
            portal_name_url = portal_url.split('.')[1] + '.' + portal_url.split('.')[2]
        except:
            portal_name = portal_url
            portal_name_url = ''

        command = "scrapy list"
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, cwd="./")
        process.wait()
        if process.returncode != 0:
            logger.error("SPIDERS NOT DETECTED")
            sys.exit()

        logger.debug(f"SCRAPING PORTAL {portal_name_url} FROM {scrapy_rs_name}...")
        if portal_name_url == 'idealista.com':
            url_last_flats = url + '?ordenado-por=fecha-publicacion-desc'
            if proxy_idealista == 'on':
                logger.debug('IDEALISTA PROXY ACTIVATED')
                os.system(
                    f"scrapy crawl -L {scrapy_log} idealista_proxy -o ./data/{scrapy_rs_name}.json -a start_urls={url_last_flats}")
            else:
                os.system(
                    f"scrapy crawl -L {scrapy_log} idealista -o ./data/{scrapy_rs_name}.json -a start_urls={url_last_flats}")

        '''#? Descomentar cuando se quieran agregar otros portales.
        elif portal_name_url == 'pisos.com':
            url_last_flats = url + '/fecharecientedesde-desc/'
            os.system(
                f"scrapy crawl -L {scrapy_log} pisoscom -o ./data/{scrapy_rs_name}.json -a start_urls={url_last_flats}")
        elif portal_name_url == 'fotocasa.es':
            os.system(f"scrapy crawl -L {scrapy_log} fotocasa -o ./data/{scrapy_rs_name}.json -a start_urls={url}")
        elif portal_name_url == 'habitaclia.com':
            # Fem crawl cridant la spider per terminal
            url_last_flats = url + '?ordenar=mas_recientes'
            os.system(
                f"scrapy crawl -L {scrapy_log} habitaclia -o ./data/{scrapy_rs_name}.json -a start_urls={url_last_flats}")
        '''
        logger.debug(f"CRAWLED {portal_name.upper()}")

    logger.debug(f"EDITING ./data/{scrapy_rs_name}.json...")
    with open(f'./data/{scrapy_rs_name}.json', 'r') as file:
        filedata = file.read()

    filedata = filedata.replace('\n][', ',')
    with open(f'./data/{scrapy_rs_name}.json', 'w') as file:
        file.write(filedata)

    # Llamamos la función que comprueba los pisos nuevos que hay, los envía por telegram y guarda en la bbdd
    check_new_flats(f"./data/{scrapy_rs_name}.json",
                    scrapy_rs_name,
                    data['min_price'],
                    data['max_price'],
                    data['telegram_chatuserID'],
                    db_client,
                    'sr_flats',
                    telegram_msg,
                    logger
    )
