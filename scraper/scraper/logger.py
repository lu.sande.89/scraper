import logging
import threading
import time
import traceback
import uuid
from inspect import stack, getframeinfo


def get_logger(name='default', level='INFO', log_path=None, log_format=None, prefix=""):
    """

    Args:
        name (str, optional): name of the file where the message will be printed. Defaults to 'default'.
        level (str, optional): _description_. Defaults to 'INFO'.
        log_path (str, optional): path of the file where the message will be printed. Defaults to None.
        log_format (str, optional): _description_. Defaults to '%(asctime)s - %(levelname)s - %(pathname)s - Line: %(lineno)d - '.
        prefix (str, optional): _description_. Defaults to "".

    Returns:
        class 'logging.Logger'
    """
    if not log_format:
        log_format='%(asctime)s - %(levelname)s - %(pathname)s - Line: %(lineno)d - '

    if log_path is None:
        log_path = '/tmp'
    logger = logging.getLogger(name)
    formatter = logging.Formatter(fmt=log_format+str(prefix)+" %(message)s")
    file_handler = logging.FileHandler(log_path + '/' + name + ".log")
    file_handler.setFormatter(formatter)
    logger.handlers = []
    logger.addHandler(file_handler)
    logger.setLevel(level)
    logger.propagate = False

    return logger


def color(msg, background=None, color=None, bold=False, underline=False):
    """Logger styles config

    Args:
        msg (str): message to print.
        background (str, optional):  background color (opciones: black,red,green,yellow,blue). Defaults to None.
        color (str, optional): text color (opciones: black,red,green,yellow,blue). Defaults to None.
        bold (bool, optional): blod text if True. Defaults to False.
        underline (bool, optional): underlined text if True. Defaults to False.

    Returns:
        str: formatted message
    """
    END='\033[0m'
    boldTag='\033[1m'
    underlinedTag='\033[4m'
    blinkTag='\033[5m'
    invertedTag='\033[7m'
    colorTags = ""
    colorDict = {
        "red":'\033[31m',
        "green":'\033[32m',
        "yellow":'\033[33m',
        "blue":'\033[34m',
    }
    backgroundDict = {
        "red":'\033[41m',
        "green":'\033[42m',
        "yellow":'\033[43m',
        "blue":'\033[44m',
    }
    if background and background in backgroundDict:
        colorTags = backgroundDict[background] + colorTags
    if color and color in colorDict:
        colorTags = colorDict[color] + colorTags
    if bold:
        colorTags = boldTag + colorTags
    if underline:
        colorTags = underlinedTag + colorTags

    msg = colorTags + "   "+str(msg)+"   "+END

    return msg


class Log():
    def __init__(
        self,
        name="default",
        level="INFO",
        prefix="",
        log_format="%(asctime)s - %(levelname)s - ",
        uid=None,
        log_path=None
    ):

        if not uid:
            self.id = str(uuid.uuid4())
        else:
            self.id = str(uid)

        log_format = str(log_format) + str(self.id)
        self.time_init = time.time()
        self.time_checkpoint = self.time_init
        self.logger = get_logger(
            name,
            level=level,
            prefix=prefix,
            log_format=log_format,
            log_path=log_path
        )
        self.logger_exception = get_logger(
            name+"_exception",
            level=level,
            prefix=prefix,
            log_format=log_format,
            log_path=log_path
        )
        self.logger_time = get_logger(
            name+"_time",
            level=level,
            prefix=prefix,
            log_format=log_format,
            log_path=log_path
        )

    def info(self, *args, **kwargs):  # Texto en verde
        path_prefix, message, level = self._get_message_level(args, kwargs, "info")
        getattr(self.logger, level)(path_prefix + color(str(message).encode('utf-8'), color="green"))

    def warning(self, *args, **kwargs):  # Texto en amarillo
        path_prefix, message, level = self._get_message_level(args, kwargs, "warning")
        getattr(self.logger, level)(path_prefix + color(str(message), color="yellow"))

    def error(self, *args, **kwargs):  # Texto en rojo
        path_prefix, message, level = self._get_message_level(args, kwargs, "error")
        getattr(self.logger, level)(path_prefix + color(str(message), color="red"))

    def inicio(self, *args, **kwargs):  # Fondo verde
        self.time_init = time.time()
        self.time_checkpoint = time.time()
        path_prefix, message, level = self._get_message_level(args, kwargs, "info")
        getattr(self.logger, level)(path_prefix + color(str(message), background="green"))

    def fin_exito(self, *args, **kwargs):  # Fondo verde
        path_prefix, message, level = self._get_message_level(args, kwargs, "info")
        self.logger_time.info(path_prefix + self._get_time_log(message, self.time_init))
        getattr(self.logger, level)(path_prefix + color(str(message), background="green"))

    def fin_warning(self, *args, **kwargs):  # Fondo amarillo
        path_prefix, message, level = self._get_message_level(args, kwargs,"warning")
        getattr(self.logger, level)(path_prefix + color(str(message), background="yellow"))

    def fin_error(self, *args, **kwargs):  # Fondo rojo
        path_prefix, message, level = self._get_message_level(args, kwargs, "error")
        getattr(self.logger, level)(path_prefix + color(str(message), background="red"))

    def debug(self, *args, **kwargs):  # Fondo azul
        path_prefix, message, level = self._get_message_level(args, kwargs, "debug")
        getattr(self.logger, level)(path_prefix + color(str(message), background="blue"))

    def set_time(self):
        self.time_checkpoint = time.time()

    def time(self, message):
        path_prefix, message, level = self._get_message_level([message], {}, "info")
        self.logger_time.info(path_prefix + self._get_time_log(
                            message,
                            self.time_checkpoint
                            )
                        )
        self.time_checkpoint = time.time()

    def exception(self, excep):  # Fondo en rojo
        path_prefix, message, level = self._get_message_level(
                                                        tuple(excep),
                                                        {},
                                                        "error")
        self.logger.error(path_prefix + color(str(message), background="red"))
        self.logger.exception(path_prefix + message)
        self.logger_exception.exception(path_prefix + message)
        excep = traceback.format_exc()
        #entorno = config.get("ENTORNO", "PRD")
        entorno ="DEV"
        if entorno != "DEV":
            try:
                t = threading.Thread(
                    target=self._send_mail,
                    args=(
                        path_prefix,
                        excep,
                        self.id,
                        entorno
                    )
                )
                t.start()
            except Exception as ex:
                self.logger_exception.exception(ex)

    def _get_time_log(self, message, time_ref):
        demora = round(time.time()-time_ref, 2)
        message = message + " demoró: " + str(demora) + " segundos"
        if demora < 1:
            return color(str(message), color="green")
        elif demora < 3:
            return color(str(message), color="yellow")
        elif demora < 5:
            return color(str(message), color="red")
        else:
            return color(str(message), background="red")

    def _get_message_level(self, args, kwargs, default_level):
        message = "".join([str(ele) for ele in args])
        caller = getframeinfo(stack()[2][0])
        filename = str(caller.filename)
        line = str(caller.lineno)
        path_prefix = filename + " - Line: " + line + " - "

        if "level" in kwargs:
            level = kwargs["level"]
        else:
            level = default_level
        return path_prefix, message, level
