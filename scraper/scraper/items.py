# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field


class RealEstateItem(Item):
    id = Field()
    price = Field()
    m2 = Field()
    rooms = Field()
    floor = Field()
    town = Field()
    neighbour = Field()
    street = Field()
    number = Field()
    type = Field()
    title = Field()
    href = Field()
    site = Field()
    post_time = Field()
    # phone = Field()
    # date = scrapy.Field()
    # discount = scrapy.Field()
    # floor_elevator = scrapy.Field()