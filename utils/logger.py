import os
import threading
import time
import traceback
import uuid
from inspect import getframeinfo, stack

from flask import current_app

from utils import logger_config as lg
from utils.logger_config import color as clg


class Log():
    def __init__(
        self,
        name="default",
        level="INFO",
        prefix="",
        log_format="%(asctime)s - %(levelname)s - ",
        uid=None,
        log_path=None
        ):

        if not uid:
            self.id = str(uuid.uuid4())
        else:
            self.id = str(uid)

        log_format = str(log_format) + str(self.id)
        self.time_init = time.time()
        self.time_checkpoint = self.time_init
        self.logger = lg.get_logger(name, level=level, prefix=prefix, log_format=log_format, log_path=log_path)
        self.logger_exception = lg.get_logger(name+"_exception", level=level, prefix=prefix, log_format=log_format, log_path=log_path)
        self.logger_time = lg.get_logger(name+"_time", level=level, prefix=prefix, log_format=log_format, log_path=log_path)

    def info(self,*args,**kwargs): # Texto en verde
        path_prefix, message,level = self._get_message_level(args,kwargs,"info")
        getattr(self.logger, level)(path_prefix + clg(str(message), color="green"))

    def warning(self,*args,**kwargs): # Texto en amarillo
        path_prefix, message,level = self._get_message_level(args,kwargs,"warning")
        getattr(self.logger, level)(path_prefix + clg(str(message), color="yellow"))

    def error(self,*args,**kwargs): # Texto en rojo
        path_prefix, message,level = self._get_message_level(args,kwargs,"error")
        getattr(self.logger, level)(path_prefix + clg(str(message), color="red"))

    def inicio(self,*args,**kwargs): # Fondo verde
        self.time_init = time.time()
        self.time_checkpoint = time.time()
        path_prefix, message,level = self._get_message_level(args,kwargs,"info")
        getattr(self.logger, level)(path_prefix + clg(str(message), background="green"))

    def fin_exito(self,*args,**kwargs): # Fondo verde
        path_prefix, message,level = self._get_message_level(args,kwargs,"info")
        self.logger_time.info(path_prefix + self._get_time_log(message,self.time_init))
        getattr(self.logger, level)(path_prefix + clg(str(message), background="green"))

    def fin_warning(self,*args,**kwargs): # Fondo amarillo
        path_prefix, message,level = self._get_message_level(args,kwargs,"warning")
        getattr(self.logger, level)(path_prefix + clg(str(message), background="yellow"))

    def fin_error(self,*args,**kwargs): # Fondo rojo
        path_prefix, message,level = self._get_message_level(args,kwargs,"error")
        getattr(self.logger, level)(path_prefix + clg(str(message), background="red"))

    def debug(self,*args,**kwargs): # Fondo azul
        path_prefix, message,level = self._get_message_level(args,kwargs,"debug")
        getattr(self.logger, level)(path_prefix + clg(str(message), background="blue"))

    def set_time(self):
        self.time_checkpoint = time.time()

    def time(self, message):
        path_prefix, message,level = self._get_message_level([message],{},"info")
        self.logger_time.info(path_prefix + self._get_time_log(message,self.time_checkpoint))
        self.time_checkpoint = time.time()

    def exception(self, excep): # Fondo en rojo
        path_prefix, message, level = self._get_message_level(tuple(excep),{},"error")
        self.logger.error(path_prefix + clg(str(message), background="red"))
        self.logger.exception(path_prefix + message)
        self.logger_exception.exception(path_prefix + message)
        excep = traceback.format_exc()
        #! entorno = current_app.config.get("ENTORNO","PRD")
        entorno = "DEV"
        if entorno != "DEV":
            try:
                t = threading.Thread(target=self._send_mail,args=(path_prefix,excep,self.id,entorno,))
                t.start()
            except Exception as ex:
                self.logger_exception.exception(ex)

    def _get_time_log(self, message, time_ref):
        demora = round(time.time()-time_ref, 2)
        message = message + " demoró: " + str(demora) + " segundos"
        if demora < 1:
            return clg(str(message), color="green")
        elif demora < 3:
            return clg(str(message), color="yellow")
        elif demora < 5:
            return clg(str(message), color="red")
        else:
            return clg(str(message), background="red")

    def _get_message_level(self, args, kwargs, default_level):
        message = "".join([str(ele) for ele in args])
        caller = getframeinfo(stack()[2][0])
        filename = str(caller.filename)
        line = str(caller.lineno)
        path_prefix = filename + " - Line: " + line + " - "

        if "level" in kwargs:
            level = kwargs["level"]
        else:
            level = default_level
        return path_prefix, message, level
