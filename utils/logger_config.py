from flask import current_app
import logging


def get_logger(name='default', level='INFO', log_path=None, log_format=None, prefix=""):
    """

    Args:
        name (str, optional): name of the file where the message will be printed. Defaults to 'default'.
        level (str, optional): _description_. Defaults to 'INFO'.
        log_path (str, optional): path of the file where the message will be printed. Defaults to None.
        log_format (str, optional): _description_. Defaults to '%(asctime)s - %(levelname)s - %(pathname)s - Line: %(lineno)d - '.
        prefix (str, optional): _description_. Defaults to "".

    Returns:
        class 'logging.Logger'
    """
    if not log_format:
        log_format='%(asctime)s - %(levelname)s - %(pathname)s - Line: %(lineno)d - '

    if log_path is None:
        log_path = current_app.config.get('LOGS_PATH', '/tmp')
    logger = logging.getLogger(name)
    formatter = logging.Formatter(fmt=log_format+str(prefix)+" %(message)s")
    file_handler = logging.FileHandler(log_path + '/' + name + ".log")
    file_handler.setFormatter(formatter)
    logger.handlers = []
    logger.addHandler(file_handler)
    logger.setLevel(level)
    logger.propagate = False

    return logger


def color(msg, background=None, color=None, bold=False, underline=False):
    """Logger styles config

    Args:
        msg (str): message to print.
        background (str, optional):  background color (opciones: black,red,green,yellow,blue). Defaults to None.
        color (str, optional): text color (opciones: black,red,green,yellow,blue). Defaults to None.
        bold (bool, optional): blod text if True. Defaults to False.
        underline (bool, optional): underlined text if True. Defaults to False.

    Returns:
        str: formatted message
    """
    END='\033[0m'
    boldTag='\033[1m'
    underlinedTag='\033[4m'
    blinkTag='\033[5m'
    invertedTag='\033[7m'
    colorTags = ""
    colorDict = {
        "red":'\033[31m',
        "green":'\033[32m',
        "yellow":'\033[33m',
        "blue":'\033[34m',
    }
    backgroundDict = {
        "red":'\033[41m',
        "green":'\033[42m',
        "yellow":'\033[43m',
        "blue":'\033[44m',
    }
    if background and background in backgroundDict:
        colorTags = backgroundDict[background] + colorTags
    if color and color in colorDict:
        colorTags = colorDict[color] + colorTags
    if bold:
        colorTags = boldTag + colorTags
    if underline:
        colorTags = underlinedTag + colorTags

    msg = colorTags + "   "+str(msg)+"   "+END

    return msg
